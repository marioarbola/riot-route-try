var ACTIONS = {
  getToken : function(){
    return localStorage.getItem('token');
  },
  setToken : function(token){
    localStorage.setItem('token',token)
  },
  isTokenValid : function(){
    var result = false;
    $.ajax({
      url: '/secure-api/check',
      dataType:'application/json',
      headers: { 'token': ACTIONS.getToken() },
      type:'GET',
      async :false,
      success:function(data){
        result = data.success;
      },
      error: function(err){
        if(err.status === 200){
          data = JSON.parse(err.responseText);
          result = data.success;
        }else{
          console.log("failed ",err);
        }
      }
    });
    return result;
  },
  login : function(email,password,callback,errorCallback){
    var user = {"email":email,"password":password};
    $.ajax({
      url: '/api/getToken/',
      type:'POST',
      dataType: 'application/json',
      data: user,
      success: function(data){
        ACTIONS.setToken(data.token);
        callback(data);
      },
      error: function(err){
        if(err.status === 200){
          data = JSON.parse(err.responseText);
          ACTIONS.setToken(data.token);
          callback(data);
        }else{
          errorCallback(err);
        }
      }
    });
  },
  logout : function(){
    ACTIONS.setToken(undefined);
  }
}
