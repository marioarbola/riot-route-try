var module = require('./modules/autenticator');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var express = require('express');
var _ = require('lodash');
var secretKey = "##XD##";
var port = 8181;
var app = express();
var router = express.Router();
var secureRouter = express.Router();

// getToken
router.post('/getToken', function(req, res) {
  var email = req.body.email ;
  var password = req.body.password ;
  //password = md5(password) ;
  var user = _.find(users, { 'email': email , 'password': password });
  if(user){
    var token = jwt.sign(user,secretKey);
    res.status(200).json({success: true,token: token , expiresIn:1417});
  }else{
    res.status(401).json({success: false,token: null , err : "username o password errati !" });
  }
} );

// SECURE ZONE
secureRouter.use( function(request,response,next){
  var token = request.body.token || request.headers['token'] ;
  if(token){
    jwt.verify(token,secretKey,function(err,decode){
      if(!err){
        next();
      }else{
        response.status(405).json({ message : "Invalid Token!" });
      }
    });
  }else{
    response.json({
      success:false,
      message : 'No token Found'
    });
  }
} );

secureRouter.get('/check', function(req, res){
  res.status(200).json({ success: true });
});

secureRouter.get('/get-data', function(req, res){
  res.json({ message: "access to data allow " });
});

// REGISTER OUR ROUTES
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/secure-api', secureRouter);
app.use('/api', router);
app.use('/mattone/', express.static('webApp'));
app.listen(port);

console.log('Service linstening on http://localhost:' + port + '/api');

var users = [
  {
    name : 'test',
    email :'test@test.com',
    password : 'abc'
  },
  {
    name : 'admin',
    email :'admin@test.com',
    password : 'admin'
  }
]
